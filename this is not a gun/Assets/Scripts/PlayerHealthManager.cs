﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerHealthManager : MonoBehaviour
{
    public int startingHealth;
    public float flashLength;

    private int currentHealth;
    private float flashCounter;
    private Renderer rend;
    private Color storedColor;

    public Image _image;
    Animator _animator;


    void Awake()
    {
        currentHealth = startingHealth;

        rend = GetComponent<Renderer>();
        storedColor = rend.material.GetColor("_Color");

        _animator = _image.GetComponent<Animator>();

    }


    void Update()
    {
        if (currentHealth<= 0)
        {
            gameObject.SetActive(false);
            
        }

        if (flashCounter > 0)
        {
            flashCounter -= Time.deltaTime;
            if (flashCounter <= 0)
            {
                rend.material.SetColor("_Color", storedColor);
            }
        }
    }

    public void HurtPlayer(int damageAmount)
    {
        currentHealth -= damageAmount;
        flashCounter = flashLength;
        rend.material.SetColor("_Color", Color.white);
    }


    IEnumerator BounceRoutine(string sceneName)
    {
        _animator.SetTrigger("BounceIn");
        yield return new WaitForSecondsRealtime(3.0f);
        SceneManager.LoadScene(sceneName);
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Enemy"))
        {
            StartCoroutine(BounceRoutine("GameOver_Scene"));
        }
    }
}
