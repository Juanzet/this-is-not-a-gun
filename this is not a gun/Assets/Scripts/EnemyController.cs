﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyController : MonoBehaviour
{

    private Rigidbody rb;

    public float moveSpeed;
    public PlayerMovement thePlayer;

    public static bool Respawn;

   
    void Awake()
    {
        rb = GetComponent<Rigidbody>();
        thePlayer = FindObjectOfType<PlayerMovement>();
        

    }

    private void Start()
    {
        transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));

        Respawn = false;

    }

    private void FixedUpdate()
    {
        rb.velocity = (transform.forward * moveSpeed);
    }

    void Update()
    {
        transform.LookAt(thePlayer.transform.position);
        
    }


    private void OnDestroy()
    {
        Respawn = true;
    }

}
