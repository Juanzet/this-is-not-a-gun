﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperScript : MonoBehaviour
{

    Animator _animator;

    bool outPaper;

    public static int constrainSet;

    public static bool destroy;

    public GameObject[] Walls;

    int SpawnCount;


    private void Awake()
    {
        _animator = GetComponent<Animator>(); 
    }

    void Start()
    {

        SpawnCount = 0;

        StartCoroutine(FadeRoutine());
        

    }

   
    void Update()
    {
        

        if (outPaper == true)
        {
            constrainSet = Random.Range(1, 3);

            outPaper = false;
        }


        if(constrainSet == 1 && SpawnCount<1)
        {
            foreach(GameObject wall in Walls)
            {
                wall.transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));
            }
            SpawnCount++;
        }

        if (constrainSet == 2 && SpawnCount < 1)
        {
            foreach (GameObject wall in Walls)
            {
                wall.transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));
            }
            SpawnCount++;
        }

        if (constrainSet == 3 && SpawnCount < 1)
        {
            foreach (GameObject wall in Walls)
            {
                wall.transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));
            }
            SpawnCount++;

        }

    }


    void FadeIn()
    {
        _animator.SetTrigger("Fadein");
        SoundManager.instancia.PausarSonido("Music");
        outPaper = false;
    }

    void FadeOut()
    {
        _animator.SetTrigger("Fadeout");
        SoundManager.instancia.ReproducirSonido("Music");
        outPaper = true;
    }


    IEnumerator FadeRoutine()
    {
        yield return new WaitForSecondsRealtime(Random.Range(4, 8));

        FadeIn();

        SoundManager.instancia.ReproducirSonido("Voice1");

        yield return new WaitForSecondsRealtime(1.0f);
        SoundManager.instancia.ReproducirSonido("Voice2");

        yield return new WaitForSecondsRealtime(2.0f);
        SoundManager.instancia.ReproducirSonido("Voice3");

        yield return new WaitForSecondsRealtime(Random.Range(2, 4));

        FadeOut();

        yield return new WaitForSecondsRealtime(1.0f);

        Start();
        

    }

    


}
