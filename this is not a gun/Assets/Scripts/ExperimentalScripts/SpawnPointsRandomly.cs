﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnPointsRandomly : MonoBehaviour
{

    public GameObject _enemy;

    float time;

    int spawncount;

    void Start()
    {

        Instantiate(_enemy, _enemy.transform.position, _enemy.transform.rotation);

    }

    
    void Update()
    {
  
        if(EnemyController.Respawn == true)
        {
            Instantiate(_enemy, _enemy.transform.position, _enemy.transform.rotation);
        }

        if(EnemyController.Respawn == true && PaperScript.constrainSet == 1)
        {
            for(int i = 0; i<2; i++)
            {
                Instantiate(_enemy, _enemy.transform.position, _enemy.transform.rotation);
            }
        }


        if (EnemyController.Respawn == true && PaperScript.constrainSet == 2)
        {
            for (int i = 0; i < 3; i++)
            {
                Instantiate(_enemy, _enemy.transform.position, _enemy.transform.rotation);
            }
        }


        if (EnemyController.Respawn == true && PaperScript.constrainSet == 3)
        {
            for (int i = 0; i < 4; i++)
            {
                Instantiate(_enemy, _enemy.transform.position, _enemy.transform.rotation);
            }
        }

    }


   
   

}
