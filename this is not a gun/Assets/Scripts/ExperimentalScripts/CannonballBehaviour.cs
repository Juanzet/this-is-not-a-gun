﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonballBehaviour : MonoBehaviour
{
    public static bool destroyBullet;

     public float bulletSpeed;


    private void Start()
    {
        destroyBullet = false;
    }

    private void Update()
    {
        transform.Translate(Vector3.forward * bulletSpeed * Time.deltaTime);

        if(PaperScript.constrainSet == 1)
        {
            bulletSpeed = 8;
        }

        if(PaperScript.constrainSet == 2)
        {
            bulletSpeed = 20;
        }

        if(PaperScript.constrainSet == 3)
        {
            bulletSpeed = 10;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") || other.gameObject.CompareTag("Wall"))
        {
            Destroy(gameObject);
        }

       

    }
    private void OnDestroy()
    {
        destroyBullet = true;
    }
}
