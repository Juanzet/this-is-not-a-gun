﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement_faster : MonoBehaviour
{

    public float speed;

    Rigidbody _rigidbody;

    private void Awake()
    {
        _rigidbody = GetComponent<Rigidbody>();
    }

    void Start()
    {
        SoundManager.instancia.ReproducirSonido("Music");
    }

    private void Update()
    {
        if(PaperScript.constrainSet == 1)
        {
            speed = 15;
        }

        if(PaperScript.constrainSet == 2)
        {
            speed = 8;
        }

        if(PaperScript.constrainSet == 3)
        {
            speed = 20;
        }
    }


    void FixedUpdate()
    {
        float horizontal = Input.GetAxisRaw("Horizontal");
        float vertical = Input.GetAxisRaw("Vertical");
        Vector3 movement = new Vector3(horizontal*speed, 0, vertical*speed);

        _rigidbody.AddForce(movement);
    }
}
