﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BounceOnWalls : MonoBehaviour
{
    public GameObject _player;

    Rigidbody _rigidbody;

    bool bounce;

    public float bounceSpeed;

    private void Awake()
    {
        _rigidbody = _player.GetComponent<Rigidbody>();
        bounce = false;
    }


    void FixedUpdate()
    {
        if(bounce == true)
        {
            _rigidbody.AddForce(_player.transform.position * -bounceSpeed,ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            bounce = true;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            bounce = false;
        }
    }
}
