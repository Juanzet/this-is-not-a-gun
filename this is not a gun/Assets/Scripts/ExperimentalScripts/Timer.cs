﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class Timer : MonoBehaviour
{

    public Text timeLeft;

    float totalTime;

    public Image _image;

    Animator _animator;

    private void Awake()
    {
        _animator = _image.GetComponent<Animator>();
    }

    void Start()
    {

        timeLeft.text = "Time left" + totalTime.ToString();
        StartCoroutine(GameTimer(60));
    }

    
    void Update()
    {
        
       
        


        if(totalTime == 0)
        {
            StartCoroutine(FadeRoutine("FinalScene"));
        }

        

    }

    IEnumerator GameTimer(float timediscount = 120)
    {
        totalTime = timediscount;

        while(totalTime > 0)
        {
            timeLeft.text = "Time left:" + " " + totalTime.ToString();
            yield return new WaitForSecondsRealtime(1.0f);
            totalTime--;


        }

       
        

    }
    IEnumerator FadeRoutine(string sceneName)
    {
        _animator.SetTrigger("Fade");
        yield return new WaitForSecondsRealtime(1.0f);
        SceneManager.LoadScene(sceneName);
    }
}
