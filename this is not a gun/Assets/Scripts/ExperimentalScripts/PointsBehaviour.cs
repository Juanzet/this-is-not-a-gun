﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsBehaviour : MonoBehaviour
{
    float addPoints;

    public static bool Spawn;

    Animator _animator;

    float time;

    

    


    private void Awake()
    {
        _animator = GetComponent<Animator>();
    }

    void Start()
    {
       

        transform.position = new Vector3(Random.Range(-8, 8), -1.23f, Random.Range(-8, 8));


    }

    
    void Update()
    {
        

        StartCoroutine(SpawnRoutine(time));

        if(time == 0)
        {
            Spawn = true;
            
        }
        

        if(Spawn == true)
        {
            
            _animator.SetBool("Spawn", true);
 
            
        }
        

        
     
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player") && Spawn == true)
        {         
            Destroy(gameObject);
            
        }
    }

    public IEnumerator SpawnRoutine(float spawntime = 3f)
    {

        while (spawntime >= 0)
        {
            time = spawntime;
            yield return new WaitForSeconds(1f);
            spawntime -= 1;
            
        }

    }

    

    private void OnDestroy()
    {
        Spawn = false;
    }

}
