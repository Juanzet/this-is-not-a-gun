﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class TransitionScript : MonoBehaviour
{
    Animator _animator;

    public float transSpeed;

    void Start()
    {
        
    }

   
    void Update()
    {
       
    }

    IEnumerator TransitionRoutine(string sceneName)
    {
        _animator.SetTrigger("BounceIn");
        yield return new WaitForSeconds(transSpeed);
        SceneManager.LoadScene(sceneName);
    }


}
