﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CreditsScript : MonoBehaviour
{

    public Text[] _text;

    private void Awake()
    {
        foreach(Text text in _text)
        {
            text.enabled = false;
        }
    }

    void Start()
    {
        StartCoroutine(Credits());
    }

   
    void Update()
    {
        
    }

    IEnumerator Credits()
    {
        yield return new WaitForSecondsRealtime(1.0f);
        _text[0].enabled = true;
        yield return new WaitForSecondsRealtime(3.0f);
        _text[1].enabled = true;
        yield return new WaitForSecondsRealtime(5.0f);
        _text[0].enabled = false;
        _text[1].enabled = false;
        _text[2].enabled = true;
        yield return new WaitForSecondsRealtime(3.0f);
        _text[2].enabled = false;
        _text[3].enabled = true;
        yield return new WaitForSecondsRealtime(2.0f);
        _text[4].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[5].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[6].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[7].enabled = true;
        yield return new WaitForSecondsRealtime(6.0f);
        _text[4].enabled = false;
        _text[5].enabled = false;
        _text[6].enabled = false;
        _text[7].enabled = false;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[8].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[9].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[10].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[11].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[12].enabled = true;
        yield return new WaitForSecondsRealtime(1.0f);
        _text[13].enabled = true;
        yield return new WaitForSecondsRealtime(8.0f);
        for(int i = 8; i < 14; i++)
        {
            _text[i].enabled = false;
        }
        _text[14].enabled = true;

        yield return new WaitForSecondsRealtime(5.0f);

        SceneManager.LoadScene("Menu_Scene");
    }

}
