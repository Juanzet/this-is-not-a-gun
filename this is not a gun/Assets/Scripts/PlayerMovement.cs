﻿using UnityEngine;
using System.Collections;

public class PlayerMovement: MonoBehaviour
{
    
    public GunController theGun;

    private Rigidbody rb;
    private Vector3 moveInput;
    private Vector3 moveVelocity;
    private Camera _camera;

    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        _camera = FindObjectOfType<Camera>();
    }

    private void Update()
    {
       

        Ray cameraRay = _camera.ScreenPointToRay(Input.mousePosition);
        Plane GroundPlane = new Plane(Vector3.up, Vector3.zero);
        float rayLength;

        if(GroundPlane.Raycast(cameraRay, out rayLength))
        {
            Vector3 pointTooLook = cameraRay.GetPoint(rayLength);
            Debug.DrawLine(cameraRay.origin, pointTooLook, Color.blue);

            transform.LookAt(new Vector3(pointTooLook.x, transform.position.y, pointTooLook.z));
        }


        if(Input.GetMouseButtonDown(0))  
            theGun.isFiring = true;
        if (Input.GetMouseButtonUp(0))
            theGun.isFiring = false;

    }

    
}
