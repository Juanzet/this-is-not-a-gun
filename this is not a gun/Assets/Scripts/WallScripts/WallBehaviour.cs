﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallBehaviour : MonoBehaviour
{
    public GameObject Cannon;

    int identity;

    bool bounce;

    bool mustBounce;

    bool spawnCannon;

    bool mustSpawn;

    Collider _collider;

    public GameObject[] _sp;

    GameObject Player;

    Rigidbody _rigidbody;

    public float bounceSpeed;

    int changeValue;

    private void Awake()
    {
        Player = GameObject.Find("Player");

        _rigidbody = Player.GetComponent<Rigidbody>();

        _collider = GetComponent<Collider>();
    }

    void Start()
    {
        identity = Random.Range(1, 3);

        transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6,6));

        changeValue = 0;
    }

    
    void Update()
    {
        
        if(identity == 1)
        {
            mustBounce = true;
        }
       
        if(identity == 2)
        {
            _collider.enabled = false;
        }

        if(identity == 3)
        {
            spawnCannon = true;
        }

        if (mustSpawn == true)
        {
            int point = Random.Range(0, 2);

            Instantiate(Cannon, _sp[point].transform.position, _sp[point].transform.rotation);
        }
       
       /* if(PaperScript.constrainSet == 1 && changeValue < 1)
        {
            transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));
            changeValue++;
        }

        if (PaperScript.constrainSet == 2 && changeValue < 1)
        {
            transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));
            changeValue++;
        }

        if (PaperScript.constrainSet == 3 && changeValue < 1)
        {
            transform.position = new Vector3(Random.Range(-7, 9), -0.7f, Random.Range(-6, 6));
            changeValue++;
        }*/
    }

    private void FixedUpdate()
    {
        if (bounce == true)
        {
            _rigidbody.AddForce(Player.transform.position * -bounceSpeed, ForceMode.Impulse);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.CompareTag("Player") && mustBounce == true)
        {
            bounce = true;
        }

        if(collision.gameObject.CompareTag("Player") && spawnCannon == true)
        {
            mustSpawn = true;
        }

    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player") && mustBounce == true)
        {
            bounce = false;
        }
    }

}
