﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuOpciones : MonoBehaviour
{
    
    public void Jugar()
    {
        SceneManager.LoadScene("Game_Scene");
    }
     
    public void Salir()
    {
        Application.Quit();
    }

    public void Menu()
    {
        SceneManager.LoadScene("Menu_Scene");
    }

    public void Controles()
    {
        SceneManager.LoadScene("Controles");
    }
    public void Creditos()
    {
        SceneManager.LoadScene("FinalScene");
    }

}
